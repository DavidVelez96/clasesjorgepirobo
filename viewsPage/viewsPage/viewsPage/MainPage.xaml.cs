﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace viewsPage
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            timePicker.Time = new TimeSpan(15, 0, 0);
        }

        private void Editor_TextChanged(object sender, TextChangedEventArgs e)
        {
            DisplayAlert("Titulo", "Text cambiado", "OK");
        }

        private void Editor_Completed(object sender, EventArgs e)
        {
            DisplayAlert("Titulo", "Text completado!!", "OK");
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            DisplayAlert("Titulo", "Ejecutando busqueda", "OK");
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            DisplayAlert("Titulo", "Buscando de forma autocompletada", "OK");
        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            valueSlider.Text = slider.Value.ToString();
        }

        private void Stepper_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            valueSteper.Text = steper.Value.ToString();
        }

        
    }
}
